const httpProxy = require('http-proxy')
const proxy = httpProxy.createProxyServer()
const API_URL = process.env.API_URL || "http://potasio/dextriggers/api"

export default {
  path: '/api',
  handler(req, res) {
    proxy.web(req, res, {
      target: API_URL
    })
  }
}