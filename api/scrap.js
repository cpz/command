import fs from 'fs';
import request from 'request'
const cachePath = "/tmp/cache"
const cookie = 'ASP.NET_SessionId=jda3ks453rrrs3ri2njho31n; .dex.siainteractive.com='+
'B655F183DF131CB57EAB1570024D19FA4EB2B44433DC15DF39931C29DE8B77A8CDBB522F7707F4193866D368A3DB39DF9C5BC3851272BEC6F191E9870EE3C4DBC25BF9E256CD49F52D37842FB82E9D647EA2D0927D2E027BF0A579AA4D5F37B2'

export default {
  path: '/data',
  handler(req, res) {

    var headers = {
      "Accept": "application/json",
      "Accept-Language": "en-US,en;q=0.9,es-AR;q=0.8,es;q=0.7,la;q=0.6",
      "Authority": "server.dexmanager.com",
      "Cache-Control": "no-cache",
      "Content-Type": "application/json",
      "Pragma": "no-cache",
      "Sec-Fetch-Mode": "cors",
      "Sec-Fetch-Site": "same-origin",
      "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
      "X-Requested-With": "XMLHttpRequest",
      'Cookie': cookie
    };
    // Configure the request
    var options = {
      url: 'https://server.dexmanager.com/DexFrontEnd/network/GetMachines?activeCust=2',
      method: 'POST',
      headers: headers,
      body: null
    };
    fs.readFile(cachePath, function read(err, data) {
      if (err) throw err;
      res.send(data)
    });
    request(options, function (error, response, body) {
      // console.log(error, response, body)
      if (!error && response.statusCode === 200) {
        fs.writeFile(cachePath, body, function(err) {
          if(err) {
              return console.log(err);
          }
        }) 
      } else {
          console.error(error);
      }
    });
  }
}