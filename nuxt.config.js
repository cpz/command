
module.exports = {
  mode: 'spa',
  // publicPath : 'triggers/',
  // : 'triggers/',
  env: process.env,
  // serverMiddleware: [
  //   '~/api/proxy',
  //   '~/api/udp'
  // ],
  server: {
    // host: '192.168.1.193',
    // port: 80
  },
  // router: {
  //   base: '/triggers/'
  // },
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      // { rel: 'stylesheet', href: '//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' },
      { rel: 'stylesheet', href: 'icons.css' },
      { rel: 'stylesheet', href: 'fonts/futura.css' },
      // { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Nunito+Sans' },
      // Hind|Josefin+Sans|Nunito+Sans|Didact+Gothic
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
  ],
  loading: {
    color: '#fff'
  },
  plugins: [
  ],
  buildModules: [
  ],
  modules: [
  ],
  build: {
    extend (config, ctx) {
    }
  }
}
