import zlib from "zlib"
import dgram from "dgram"

var host = "239.254.0.1", port = 9600
var client = dgram.createSocket("udp4")

client.on("message", function(msg, rinfo) {
  let x = zlib.gunzipSync(msg).toString()
  console.log("Rta:", rinfo, x)
})

client.bind(port, function() {
  client.setMulticastTTL(128);
  client.addMembership(host);
})

export default {
  path: '/send',
  handler(req, res) {
    let triggers = {
      MessageId: `512db6f8-707f-46e2-9ad6${Math.random()}`,
      Triggers: req.body
    }
    var message = () => zlib.gzipSync(JSON.stringify(triggers))
    res.json(triggers)

    let msg = message();
    client.send(msg, 0, msg.length, port, host)
  }
}